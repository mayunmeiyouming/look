## 电梯调度算法

> https://www.jianshu.com/p/083bc9d897ee

> https://www.cnblogs.com/jianyungsun/archive/2011/03/16/1986439.html

### 扫描算法(SCAN)

扫描算法(SCAN)是一种按照楼层顺序依次服务请求，它让电梯在最底层和最顶层之间连续往返运行，在运行过程中响应处在于电梯运行方向相同的各楼层上的请求。它寻找楼层的方式，效率比较高，但它是一个非实时算法。扫描算法较好地解决了电梯移动的问题，在这个算法中，每个电梯响应乘客请求使乘客获得服务的次序是由其发出请求的乘客的位置与当前电梯位置之间的距离来决定的，所有的与电梯运行方向相同的乘客的请求在一次电梯向上运行或向下运行的过程中完成，免去了电梯频繁的来回移动。

扫描算法的平均响应时间比最短寻找楼层时间优先算法长，但是响应时间方差比最短寻找楼层时间优先算法小，从统计学角度来讲，扫描算法要比最短寻找楼层时间优先算法稳定。

### LOOK 算法

LOOK算法是扫描算法的一种改进。对LOOK算法而言，电梯同样在最底层和最顶层之间运行。但当LOOK算法发现电梯所移动的方向上不再有请求时立即改变运行方向，而扫描算法则需要移动到最底层或者最顶层时才改变运行方向。

```
type Elevator struct {
	State    int // 1 上行中 2 下行中 3 静止
	Position int // 电梯当前位置
	low      int
	high     int
}

type InTask struct {
	CurrentPosition int // 在第几层
	Direction       int // 1 向上 2 向下
}

type OutTask struct {
	GoPosition int
}

type smallPriorityQueue struct { // 位置小，在队列前面
	InTasks []*InTask
}

type bigPriorityQueue struct { // 位置大，在队列前面
	InTasks []*InTask
}

var elevator = &Elevator{
	State:    3,
	Position: 1,
	low:      1,
	high:     20,
}

var inDownRecvQueue = &bigPriorityQueue{}

var inDownWaitQueue = &smallPriorityQueue{}

var inUpRecvQueue = &smallPriorityQueue{}

var inUpWaitQueue = &bigPriorityQueue{}

var outUpQueue = &smallPriorityQueue{}

var outDownQueue = &bigPriorityQueue{}

func main() {
	//进电梯命令
	go func() {
		currentPosition := 1 // 在第几层
		direction := 1       // 1 向上 2 向下
		for {
			fmt.Scanln(&currentPosition, &direction)
			inTask := InTask{
				CurrentPosition: currentPosition, // 在第几层
				Direction:       direction,       // 1 向上 2 向下
			}
			if direction == elevator.State {
				if direction == 2 { // 都向下
					if currentPosition < elevator.Position {
						inDownRecvQueue.push(inTask)
					} else {
						inDownWaitQueue.push(inTask)
					}
				} else if direction == 1 {
					if currentPosition < elevator.Position {
						inUpWaitQueue.push(inTask)
					} else {
						inUpRecvQueue.push(inTask)
					}
				}
			} else {
				if elevator.State == 3 { //电梯静止
					if currentPosition < elevator.Position {
						inDownRecvQueue.push(inTask)
					} else {
						inUpRecvQueue.push(inTask)
					}
				} else {
					if elevator.State == 1 && direction == 2 {
						inDownWaitQueue.push(inTask)
					} else if elevator.State == 2 && direction == 1 {
						inUpWaitQueue.push(inTask)
					}
				}
			}

		}
	}()

	// 出电梯
	go func() {
		goPosition := 1
		for {
			fmt.Scanln(&goPosition)
			outTask := &OutTask{
				GoPosition: goPosition,
			}
			if elevator.State == 1 {
				outUpQueue.push(outTask)
			} else if elevator.State == 2 {
				outDownQueue.push(outTask)
			} else {
				if elevator.Position < goPosition {
					outUpQueue.push(outTask)
				} else {
					outDownQueue.push(outTask)
				}
			}
		}
	}()

	// 控制单元
	go func() {
		for {
			if elevator.State == 3 {
				if inUpRecvQueue.isEmpty() == false { //有人
					elevator.State = 1 //电梯上行
				} else if inDownRecvQueue.isEmpty() == false {
					elevator.State = 2 //电梯下行
				}
			} else if elevator.State == 1 { //电梯上行
				if inUpRecvQueue.isEmpty() == true && outUpQueue.isEmpty() == true { //上行没人
					if inDownWaitQueue.isEmpty() == false { //下行有人等待
						elevator.State = 2
					} else {
						elevator.State = 3
					}
				}
			} else if elevator.State == 2 { //电梯下行
				if inDownRecvQueue.isEmpty() == true && outDownQueue.isEmpty() == true { //下行没人
					if inUpWaitQueue.isEmpty() == false { //上行有人等待
						elevator.State = 1
					} else {
						elevator.State = 3
					}
				}
			}
		}
	}()

	// 移动单元
	go func() {
		for {
			if elevator.State == 1 {  //电梯上行
				inTask1 := inUpRecvQueue().peek()
				outTask1 := outUpQueue().peek()
				if inTask1.CurrentPosition <= outTask.GoPosition { //电梯移动到最近的位置
					elevator.Position = inTask1.CurrentPosition 
					for {
						if inUpRecvQueue().peek().Position == elevator.Position {
							inUpRecvQueue().poll() //弹出队列
						} else {
							break
						}
					}

					for {
						if outUpQueue().peek().GoPosition == elevator.Position {
							outUpQueue().poll() //弹出队列
						} else {
							break
						}
					}
				}
			}
		}
	}()
}
```