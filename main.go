package main

import (
	"time"
	"fmt"
)

// https://www.jianshu.com/p/083bc9d897ee

type Elevator struct {
	State    int // 1 上行中 2 下行中 3 静止
	Position int // 电梯当前位置
	low      int
	high     int
}

type InTask struct {
	CurrentPosition int // 在第几层
	Direction       int // 1 向上 2 向下
}

type OutTask struct {
	GoPosition int
}

type smallPriorityQueue struct { // 位置小，在队列前面
	InTasks []*InTask
}

type bigPriorityQueue struct { // 位置大，在队列前面
	InTasks []*InTask
}

var elevator = &Elevator{
	State:    3,
	Position: 1,
	low:      1,
	high:     20,
}

var inDownRecvQueue = &bigPriorityQueue{}

var inDownWaitQueue = &smallPriorityQueue{}

var inUpRecvQueue = &smallPriorityQueue{}

var inUpWaitQueue = &bigPriorityQueue{}

var outUpQueue = &smallPriorityQueue{}

var outDownQueue = &bigPriorityQueue{}

func main() {
	//进电梯命令
	go func() {
		currentPosition := 1 // 在第几层
		direction := 1       // 1 向上 2 向下
		for {
			fmt.Scanln(&currentPosition, &direction)
			inTask := InTask{
				CurrentPosition: currentPosition, // 在第几层
				Direction:       direction,       // 1 向上 2 向下
			}
			if direction == elevator.State {
				if direction == 2 { // 都向下
					if currentPosition < elevator.Position {
						inDownRecvQueue.push(inTask)
					} else {
						inDownWaitQueue.push(inTask)
					}
				} else if direction == 1 {
					if currentPosition < elevator.Position {
						inUpWaitQueue.push(inTask)
					} else {
						inUpRecvQueue.push(inTask)
					}
				}
			} else {
				if elevator.State == 3 { //电梯静止
					if currentPosition < elevator.Position {
						inDownRecvQueue.push(inTask)
					} else {
						inUpRecvQueue.push(inTask)
					}
				} else {
					if elevator.State == 1 && direction == 2 {
						inDownWaitQueue.push(inTask)
					} else if elevator.State == 2 && direction == 1 {
						inUpWaitQueue.push(inTask)
					}
				}
			}

		}
	}()

	// 出电梯
	go func() {
		goPosition := 1
		for {
			fmt.Scanln(&goPosition)
			outTask := &OutTask{
				GoPosition: goPosition,
			}
			if elevator.State == 1 {
				outUpQueue.push(outTask)
			} else if elevator.State == 2 {
				outDownQueue.push(outTask)
			} else {
				if elevator.Position < goPosition {
					outUpQueue.push(outTask)
				} else {
					outDownQueue.push(outTask)
				}
			}
		}
	}()

	// 控制单元
	go func() {
		for {
			if elevator.State == 3 {
				if inUpRecvQueue.isEmpty() == false { //有人
					elevator.State = 1 //电梯上行
				} else if inDownRecvQueue.isEmpty() == false {
					elevator.State = 2 //电梯下行
				}
			} else if elevator.State == 1 { //电梯上行
				if inUpRecvQueue.isEmpty() == true && outUpQueue.isEmpty() == true { //上行没人
					if inDownWaitQueue.isEmpty() == false { //下行有人等待
						elevator.State = 2
					} else {
						elevator.State = 3
					}
				}
			} else if elevator.State == 2 { //电梯下行
				if inDownRecvQueue.isEmpty() == true && outDownQueue.isEmpty() == true { //下行没人
					if inUpWaitQueue.isEmpty() == false { //上行有人等待
						elevator.State = 1
					} else {
						elevator.State = 3
					}
				}
			}
		}
	}()

	// 移动单元
	go func() {
		for {
			if elevator.State == 1 {  //电梯上行
				inTask1 := inUpRecvQueue().peek()
				outTask1 := outUpQueue().peek()
				if inTask1.CurrentPosition <= outTask.GoPosition { //电梯移动到最近的位置
					elevator.Position = inTask1.CurrentPosition 
					for {
						if inUpRecvQueue().peek().Position == elevator.Position {
							inUpRecvQueue().poll() //弹出队列
						} else {
							break
						}
					}

					for {
						if outUpQueue().peek().GoPosition == elevator.Position {
							outUpQueue().poll() //弹出队列
						} else {
							break
						}
					}
				}
			}
		}
	}()
}